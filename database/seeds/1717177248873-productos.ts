import { CategoriaProducto } from '@/application/categoria-producto/entity/categoria-producto.entity'
import { CategoriaTienda } from '@/application/categoria-tienda/entity'
import { Producto } from '@/application/product/entity'
import { Tienda } from '@/application/tienda/entity'
import { Unidad } from '@/application/unidad/entity'
import { USUARIO_SISTEMA } from '@/common/constants'
import { MigrationInterface, QueryRunner } from 'typeorm'

export class Productos1717177248873 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const tienda = [
      {
        direccion: 'Prado',
        telefono: '674852136', //TextService.textToUuid(RolEnum.ADMINISTRADOR),
        gerente: 'Juan Diaz', //TextService.textToUuid('ADMINISTRADOR'),
      },
      {
        direccion: 'Villa Asuncion',
        telefono: '69258469', //TextService.textToUuid(RolEnum.ADMINISTRADOR),
        gerente: 'Pedro Murillo', //TextService.textToUuid('ADMINISTRADOR'),
      },
    ]
    const tiendas = tienda.map((item) => {
      return new Tienda({
        direccion: item.direccion,
        telefono: item.telefono,
        gerente: item.gerente,
        estado: 'ACTIVO',
        transaccion: 'SEEDS',
        usuarioCreacion: USUARIO_SISTEMA,
      })
    })
    await queryRunner.manager.save(tiendas)

    const categoriasTiendas = [
      {
        nombre: 'Ropas',
        descripcion: 'Ropas para la venta',
      },
      {
        nombre: 'Articulos',
        descripcion: 'Diferentes tipos de articulos',
      },
    ]
    const parametrosTienda = categoriasTiendas.map((item) => {
      return new CategoriaTienda({
        nombre: item.nombre,
        descripcion: item.descripcion,
        idTienda: 1,
        estado: 'ACTIVO',
        transaccion: 'SEEDS',
        usuarioCreacion: USUARIO_SISTEMA,
      })
    })
    await queryRunner.manager.save(parametrosTienda)

    const items = [
      { nombre: 'unidad', descripcion: 'Unidad' },
      { nombre: 'par', descripcion: 'Par' },
      { nombre: 'docena', descripcion: 'Docena' },
      // Puedes agregar más unidades según sea necesario
    ]
    const parametrosItems = items.map((item) => {
      return new Unidad({
        nombre: item.nombre,
        descripcion: item.descripcion,
        estado: 'ACTIVO',
        transaccion: 'SEEDS',
        usuarioCreacion: USUARIO_SISTEMA,
      })
    })
    await queryRunner.manager.save(parametrosItems)

    const categorias = [
      { codigo: 'ROP', nombre: 'Ropa', descripcion: 'Ropa y Accesorios' },
      { codigo: 'CAL', nombre: 'Calzado', descripcion: 'Calzado' },
      {
        codigo: 'ELE',
        nombre: 'Electrónica',
        descripcion: 'Electrónica de Consumo',
      },
      {
        codigo: 'PAP',
        nombre: 'Papelería',
        descripcion: 'Artículos de Papelería',
      },
      { codigo: 'ACC', nombre: 'Accesorios', descripcion: 'Accesorios Varios' },
    ]

    const parametros = categorias.map((item) => {
      return new CategoriaProducto({
        codigo: item.codigo,
        nombre: item.nombre,
        descripcion: item.descripcion,
        estado: 'ACTIVO',
        transaccion: 'SEEDS',
        usuarioCreacion: USUARIO_SISTEMA,
        idCategoriaTienda: 1,
      })
    })

    await queryRunner.manager.save(parametros)

    const productos = [
      {
        nombre: 'Camisa Polo',
        codigo: 'C-21313',
        precio: 25.99,
        descripcion: 'Camisa de algodón estilo polo',
        stock: 2,
        stock_min: 10,
        stock_max: 100,
        imagen: 'image_default.png',
        categoriaId: 1, // Utilizamos el ID de categoría en lugar del objeto completo
        unidadId: 1, // Utilizamos el ID de unidad en lugar del objeto completo
      },
      {
        nombre: 'Pantalones Vaqueros',
        codigo: 'PV-5423',
        precio: 35.5,
        descripcion: 'Pantalones vaqueros ajustados',
        stock: 0,
        stock_min: 15,
        stock_max: 80,
        imagen: 'image_default.png',
        categoriaId: 1,
        unidadId: 1,
      },
      {
        nombre: 'Zapatillas Deportivas',
        codigo: 'ZD-1213',
        precio: 49.99,
        descripcion: 'Zapatillas deportivas para correr',
        stock: 9,
        stock_min: 5,
        stock_max: 50,
        imagen: 'image_default.png',
        categoriaId: 2,
        unidadId: 2,
      },
      {
        nombre: 'Teclado Inalámbrico',
        codigo: 'TI-123',
        precio: 39.99,
        descripcion: 'Teclado inalámbrico para PC',
        stock: 8,
        stock_min: 8,
        stock_max: 40,
        imagen: 'image_default.png',
        categoriaId: 3,
        unidadId: 1,
      },
      {
        nombre: 'Libreta de Notas',
        codigo: 'LN-015',
        precio: 4.5,
        descripcion: 'Libreta de notas tamaño A5',
        stock: 7,
        stock_min: 20,
        stock_max: 150,
        imagen: 'image_default.png',
        categoriaId: 4,
        unidadId: 1,
      },
      {
        nombre: 'Cargador USB',
        codigo: 'CU-sad123',
        precio: 9.99,
        descripcion: 'Cargador USB para dispositivos móviles',
        stock: 1,
        stock_min: 10,
        stock_max: 80,
        imagen: 'image_default.png',
        categoriaId: 3,
        unidadId: 1,
      },
      {
        nombre: 'Camisa Polo',
        codigo: 'CP-0215',
        precio: 2.99,
        descripcion: 'Botella de agua reutilizable de 500ml',
        stock: 5,
        stock_min: 30,
        stock_max: 200,
        imagen: 'image_default.png',
        categoriaId: 5,
        unidadId: 1,
      },
    ]
    const saveProductos = productos.map((item) => {
      return new Producto({
        nombre: item.nombre,
        codigo: item.codigo,
        precio: item.precio,
        descripcion: item.descripcion,
        stock: item.stock,
        stock_min: item.stock_min,
        stock_max: item.stock_max,
        imagen: item.imagen,
        idCategoria: item.categoriaId,
        idUnidad: item.unidadId,
        estado: 'ACTIVO',
        transaccion: 'SEEDS',
        usuarioCreacion: USUARIO_SISTEMA,
      })
    })
    await queryRunner.manager.save(saveProductos)
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
