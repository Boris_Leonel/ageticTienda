import { UtilService } from '@/common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '@/common/entity/auditoria.entity'
import { DetalleCarritoEstado } from '../constant'
import { Producto } from '@/application/product/entity'
import { Carrito } from '@/application/carrito/entity'

dotenv.config()

@Check(UtilService.buildStatusCheck(DetalleCarritoEstado))
@Entity({ name: 'detalles_carrito', schema: process.env.DB_SCHEMA })
export class DetalleCarrito extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Detalles de Carrito',
  })
  id: string

  @Column({
    length: 15,
    type: 'varchar',
    comment: 'Cantidad de un producto',
  })
  cantidad: string

  @Column({
    name: 'id_producto',
    type: 'bigint',
    nullable: false,
    comment: 'clave foránea que referencia la tabla productos',
  })
  idProducto: number

  @Column({
    name: 'id_carrito',
    type: 'bigint',
    nullable: false,
    comment: 'clave foránea que referencia la tabla carritos',
  })
  idCarrito: number

  @ManyToOne(() => Producto, (producto) => producto.detalleCarrito, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_producto',
    referencedColumnName: 'id',
  })
  producto: Producto

  @ManyToOne(() => Carrito, (carrito) => carrito.detalleCarrito, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_carrito',
    referencedColumnName: 'id',
  })
  carrito: Carrito

  constructor(data?: Partial<DetalleCarrito>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || DetalleCarritoEstado.ACTIVO
  }
}
