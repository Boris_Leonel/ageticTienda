import { IsNotEmpty } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class ActualizarParametroDto {
  @ApiProperty({ example: '2' })
  @IsNotEmpty()
  cantidad: string

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  idProducto: string

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  idCarrito: string

  @ApiProperty({ example: 'ACTIVO' })
  estado?: string
}
