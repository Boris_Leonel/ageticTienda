import { IsNotEmpty, IsOptional } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class CrearParametroDto {
  @ApiProperty({ example: '2' })
  @IsNotEmpty()
  cantidad: string

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  idProducto: string

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  idCarrito: string

  @ApiProperty({ example: 'ACTIVO' })
  @IsOptional()
  estado?: string
}

export class RespuestaCrearDetalleCarritoDto {
  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  id: string
}
