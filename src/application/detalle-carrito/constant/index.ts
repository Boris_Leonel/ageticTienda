import { Status } from '@/common/constants'

export enum DetalleCarritoEstado {
  ACTIVO = Status.ACTIVE,
  INACTIVO = Status.INACTIVE,
}
