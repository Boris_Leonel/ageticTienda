import { Module } from '@nestjs/common'
import { ParametroModule } from './parametro/parametro.module'
import { ProductoModule } from './product/producto.module'
import { CategoriaProductoModule } from './categoria-producto/categoriaProducto.module'
import { UnidadModule } from './unidad/unidad.module'

@Module({
  imports: [
    ParametroModule,
    ProductoModule,
    CategoriaProductoModule,
    UnidadModule,
  ],
})
export class ApplicationModule {}
