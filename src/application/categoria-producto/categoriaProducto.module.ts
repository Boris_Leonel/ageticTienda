import { Module } from '@nestjs/common'
import { CategoriaProductoController } from '@/application/categoria-producto/controller/categoriaProducto.controller'
import { CategoriaProductoService } from '@/application/categoria-producto/service/parametro.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { CategoriaProductoRepository } from '@/application/categoria-producto/repository/categoria-producto.repository'
import { CategoriaProducto } from '@/application/categoria-producto/entity/categoria-producto.entity'

@Module({
  controllers: [CategoriaProductoController],
  providers: [CategoriaProductoService, CategoriaProductoRepository],
  imports: [TypeOrmModule.forFeature([CategoriaProducto])],
})
export class CategoriaProductoModule {}
