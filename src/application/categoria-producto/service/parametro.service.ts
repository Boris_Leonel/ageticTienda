import { BaseService } from '@/common/base/base-service'
import { Inject, Injectable } from '@nestjs/common'
import { PaginacionQueryDto } from '@/common/dto/paginacion-query.dto'
import { CategoriaProductoRepository } from '../repository/categoria-producto.repository'

@Injectable()
export class CategoriaProductoService extends BaseService {
  constructor(
    @Inject(CategoriaProductoRepository)
    private categoriaProductoRepositorio: CategoriaProductoRepository
  ) {
    super()
  }

  // async crear(parametroDto: CrearParametroDto, usuarioAuditoria: string) {
  //   const parametroRepetido = await this.parametroRepositorio.buscarCodigo(
  //     parametroDto.codigo
  //   )

  //   if (parametroRepetido) {
  //     throw new ConflictException(Messages.REPEATED_PARAMETER)
  //   }

  //   return await this.parametroRepositorio.crear(parametroDto, usuarioAuditoria)
  // }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    return await this.categoriaProductoRepositorio.listar(paginacionQueryDto)
  }

  // async listarPorGrupo(grupo: string) {
  //   return await this.parametroRepositorio.listarPorGrupo(grupo)
  // }

  // async actualizarDatos(
  //   id: string,
  //   parametroDto: ActualizarParametroDto,
  //   usuarioAuditoria: string
  // ) {
  //   const parametro = await this.parametroRepositorio.buscarPorId(id)
  //   if (!parametro) {
  //     throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
  //   }
  //   await this.parametroRepositorio.actualizar(
  //     id,
  //     parametroDto,
  //     usuarioAuditoria
  //   )
  //   return { id }
  // }

  // async activar(idParametro: string, usuarioAuditoria: string) {
  //   const parametro = await this.parametroRepositorio.buscarPorId(idParametro)
  //   if (!parametro) {
  //     throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
  //   }
  //   const parametroDto = new ActualizarParametroDto()
  //   parametroDto.estado = ParametroEstado.ACTIVO
  //   await this.parametroRepositorio.actualizar(
  //     idParametro,
  //     parametroDto,
  //     usuarioAuditoria
  //   )
  //   return {
  //     id: idParametro,
  //     estado: parametroDto.estado,
  //   }
  // }

  // async inactivar(idParametro: string, usuarioAuditoria: string) {
  //   const parametro = await this.parametroRepositorio.buscarPorId(idParametro)
  //   if (!parametro) {
  //     throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
  //   }
  //   const parametroDto = new ActualizarParametroDto()
  //   parametroDto.estado = ParametroEstado.INACTIVO
  //   await this.parametroRepositorio.actualizar(
  //     idParametro,
  //     parametroDto,
  //     usuarioAuditoria
  //   )
  //   return {
  //     id: idParametro,
  //     estado: parametroDto.estado,
  //   }
  // }
}
