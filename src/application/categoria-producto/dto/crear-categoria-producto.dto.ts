import { IsNotEmpty, IsOptional } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class CrearCategoriaDto {
  @ApiProperty({ example: 'RP-002' })
  @IsNotEmpty()
  codigo: string

  @ApiProperty({ example: 'Ropas' })
  @IsNotEmpty()
  nombre: string

  @ApiProperty({ example: 'Tipos defierentes de ropa' })
  @IsNotEmpty()
  descripcion: string

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  idCategoriaTienda?: string

  @ApiProperty({ example: 'ACTIVO' })
  @IsOptional()
  estado?: string
}

export class RespuestaCrearParametroDto {
  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  id: string
}
