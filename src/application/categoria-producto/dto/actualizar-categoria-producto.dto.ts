import { IsNotEmpty } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class ActualizarCategoriaDto {
  @ApiProperty({ example: 'RP-002' })
  codigo: string

  @ApiProperty({ example: 'Ropas' })
  nombre: string

  @ApiProperty({ example: 'Tipos defierentes de ropa' })
  descripcion: string

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  idCategoriaTienda?: string

  @ApiProperty({ example: 'ACTIVO' })
  estado?: string
}
