import { UtilService } from '@/common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '@/common/entity/auditoria.entity'
import { CategoriaProductoEstado } from '../constant'
import { Producto } from '@/application/product/entity'
import { CategoriaTienda } from '@/application/categoria-tienda/entity/categoria-tienda.entity'

dotenv.config()

@Check(UtilService.buildStatusCheck(CategoriaProductoEstado))
@Entity({ name: 'categorias_producto', schema: process.env.DB_SCHEMA })
export class CategoriaProducto extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla categoría',
  })
  id: string

  @Column({
    length: 15,
    type: 'varchar',
    unique: true,
    comment: 'Código de categoría',
  })
  codigo: string

  @Column({
    length: 50,
    type: 'varchar',
    comment: 'Nombre de categoría',
  })
  nombre: string

  @Column({
    length: 255,
    type: 'varchar',
    comment: 'Descripción de categoría',
  })
  descripcion: string

  @Column({
    name: 'id_categoria_tienda',
    type: 'bigint',
    nullable: false,
    comment: 'clave foránea que referencia la tabla categoria tienda',
  })
  idCategoriaTienda: number

  @OneToMany(() => Producto, (producto) => producto.categoria)
  productos: Producto[]

  @ManyToOne(
    () => CategoriaTienda,
    (categoriaTienda) => categoriaTienda.categoriaProducto,
    {
      nullable: false,
    }
  )
  @JoinColumn({
    name: 'id_categoria_tienda',
    referencedColumnName: 'id',
  })
  categoriaTienda: CategoriaTienda

  constructor(data?: Partial<CategoriaProducto>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || CategoriaProductoEstado.ACTIVO
  }
}
