import { Status } from '@/common/constants'

export enum CategoriaProductoEstado {
  ACTIVO = Status.ACTIVE,
  INACTIVO = Status.INACTIVE,
}
