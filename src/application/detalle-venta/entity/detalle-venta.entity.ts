import { UtilService } from '@/common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '@/common/entity/auditoria.entity'
import { DetalleVentaEstado } from '../constant'
import { Producto } from '@/application/product/entity'
import { Venta } from '@/application/venta/entity/venta.entity'

dotenv.config()

@Check(UtilService.buildStatusCheck(DetalleVentaEstado))
@Entity({ name: 'detalles_venta', schema: process.env.DB_SCHEMA })
export class DetalleVenta extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla detalle venta',
  })
  id: string

  @Column({
    length: 15,
    type: 'varchar',
    unique: true,
    comment: 'Cantidad del detalle',
  })
  cantidad: string

  @Column({
    length: 15,
    type: 'varchar',
    unique: true,
    comment: 'Precio por cantidad y precio producto',
  })
  precio: string

  @Column({
    length: 15,
    type: 'varchar',
    unique: true,
    comment: 'Descuento de venta',
  })
  descuento: string

  @Column({
    name: 'id_producto',
    type: 'bigint',
    nullable: false,
    comment: 'clave foránea que referencia la tabla productos',
  })
  idProducto: number

  @Column({
    name: 'id_venta',
    type: 'bigint',
    nullable: false,
    comment: 'clave foránea que referencia la tabla ventas',
  })
  idVenta: number

  @ManyToOne(() => Producto, (producto) => producto.detalleVentas, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_producto',
    referencedColumnName: 'id',
  })
  producto: Producto

  @ManyToOne(() => Venta, (ingreso) => ingreso.detalleVenta, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_venta',
    referencedColumnName: 'id',
  })
  venta: Venta

  constructor(data?: Partial<DetalleVenta>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || DetalleVentaEstado.ACTIVO
  }
}
