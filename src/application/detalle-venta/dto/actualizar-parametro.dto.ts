import { IsNotEmpty } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class ActualizarDetalleVentaDto {
  @ApiProperty({ example: '2' })
  @IsNotEmpty()
  cantidad: string

  @ApiProperty({ example: '13.99' })
  @IsNotEmpty()
  precio: string

  @ApiProperty({ example: '10' })
  @IsNotEmpty()
  descuento: string

  @ApiProperty({ example: '1' })
  idProducto: string

  @ApiProperty({ example: '1' })
  idVenta: string

  @ApiProperty({ example: 'ACTIVO' })
  estado?: string
}
