import { IsNotEmpty, IsOptional } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class CrearDetalleVentaDto {
  @ApiProperty({ example: '2' })
  @IsNotEmpty()
  cantidad: string

  @ApiProperty({ example: '13.99' })
  @IsNotEmpty()
  precio: string

  @ApiProperty({ example: '10' })
  @IsNotEmpty()
  descuento: string

  @ApiProperty({ example: '1' })
  idProducto: string

  @ApiProperty({ example: '1' })
  idVenta: string

  @ApiProperty({ example: 'ACTIVO' })
  @IsOptional()
  estado?: string
}

export class RespuestaCrearParametroDto {
  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  id: string
}
