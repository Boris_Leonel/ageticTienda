import { Status } from '@/common/constants'

export enum DetalleVentaEstado {
  ACTIVO = Status.ACTIVE,
  INACTIVO = Status.INACTIVE,
}
