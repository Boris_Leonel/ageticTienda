import { UtilService } from '@/common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '@/common/entity/auditoria.entity'
import { Tienda } from '@/application/tienda/entity'
import { Usuario } from '@/core/usuario/entity/usuario.entity'
import { CarritoEstado } from '../constant'
import { DetalleCarrito } from '@/application/detalle-carrito/entity'

dotenv.config()

@Check(UtilService.buildStatusCheck(CarritoEstado))
@Entity({ name: 'carritos', schema: process.env.DB_SCHEMA })
export class Carrito extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Carritos',
  })
  id: string

  @ManyToOne(() => Usuario, (usuario) => usuario.carritos, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_usuario',
    referencedColumnName: 'id',
  })
  usuario: Usuario

  @ManyToOne(() => Tienda, (tienda) => tienda.carritos, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_tienda',
    referencedColumnName: 'id',
  })
  tienda: Tienda

  @OneToMany(() => DetalleCarrito, (detallesCarrito) => detallesCarrito.carrito)
  detalleCarrito: DetalleCarrito[]

  constructor(data?: Partial<Carrito>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || CarritoEstado.ACTIVO
  }
}
