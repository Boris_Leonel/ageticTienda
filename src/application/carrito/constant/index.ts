import { Status } from '@/common/constants'

export enum CarritoEstado {
  ACTIVO = Status.ACTIVE,
  INACTIVO = Status.INACTIVE,
}
