import { IsNotEmpty } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class ActualizarParametroDto {
  @ApiProperty({ example: 'unidades' })
  @IsNotEmpty()
  @ApiProperty({ example: 'litros' })
  nombre: string

  @ApiProperty({ example: 'Unidad elegida' })
  @IsNotEmpty()
  descripcion: string

  @ApiProperty({ example: 'ACTIVO' })
  estado?: string
}
