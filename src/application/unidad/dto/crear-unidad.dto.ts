import { IsNotEmpty, IsOptional } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class CrearParametroDto {
  @ApiProperty({ example: 'unidades' })
  @IsNotEmpty()
  @ApiProperty({ example: 'litros' })
  nombre: string

  @ApiProperty({ example: 'Unidad elegida' })
  @IsNotEmpty()
  descripcion: string

  @ApiProperty({ example: 'ACTIVO' })
  @IsOptional()
  estado?: string
}

export class RespuestaCrearUnidadDto {
  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  id: string
}
