import { Status } from '@/common/constants'

export enum UnidadEstado {
  ACTIVO = Status.ACTIVE,
  INACTIVO = Status.INACTIVE,
}
