import { Module } from '@nestjs/common'
import { UnidadController } from '@/application/unidad/controller/unidad.controller'
import { UnidadService } from '@/application/unidad/service/unidad.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UnidadRepository } from '@/application/unidad/repository/unidad.repository'
import { Unidad } from '@/application/unidad/entity/unidad.entity'

@Module({
  controllers: [UnidadController],
  providers: [UnidadService, UnidadRepository],
  imports: [TypeOrmModule.forFeature([Unidad])],
})
export class UnidadModule {}
