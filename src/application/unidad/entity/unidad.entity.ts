import { UtilService } from '@/common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '@/common/entity/auditoria.entity'
import { Producto } from '@/application/product/entity'
import { UnidadEstado } from '../constant'

dotenv.config()

@Check(UtilService.buildStatusCheck(UnidadEstado))
@Entity({ name: 'unidades', schema: process.env.DB_SCHEMA })
export class Unidad extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Unidades',
  })
  id: string

  @Column({ length: 50, type: 'varchar', comment: 'Nombre de unidad' })
  nombre: string

  @Column({ length: 255, type: 'varchar', comment: 'Descripción de unidad' })
  descripcion: string

  @OneToMany(() => Producto, (producto) => producto.unidad)
  productos: Producto[]

  constructor(data?: Partial<Unidad>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || UnidadEstado.ACTIVO
  }
}
