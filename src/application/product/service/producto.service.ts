import * as fs from 'fs'

import { BaseService } from '@/common/base/base-service'
import {
  ConflictException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common'
import { PaginacionQueryDto } from '@/common/dto/paginacion-query.dto'
import { Messages } from '@/common/constants/response-messages'
import { ProductoEstado } from '../constant'
import { ProductoRepository } from '../repository/producto.repository'
import { CrearProductoDto } from '../dto/crear-producto.dto'
import { FileService } from '@/common/lib/file.service'
import { extname, join } from 'path'
import { ActualizarProductoDto } from '../dto/actualizar-producto.dto'

@Injectable()
export class ProductService extends BaseService {
  constructor(
    @Inject(ProductoRepository)
    private productRepository: ProductoRepository,
    private fileService: FileService
  ) {
    super()
  }

  async create(
    images: Array<Express.Multer.File>,
    productoDto: CrearProductoDto,
    usuarioAuditoria: string
  ) {
    const productoRepetido = await this.productRepository.buscarCodigo(
      productoDto.codigo
    )
    if (productoRepetido) {
      throw new ConflictException(Messages.REPEATED_PRODUCT)
    }
    let nombreImagen = ['image_default.png']
    if (images) {
      nombreImagen = await this.fileService.saveImages(images)
    }
    return await this.productRepository.crear(
      productoDto,
      usuarioAuditoria,
      nombreImagen[0]
    )
  }

  async list(paginacionQueryDto: PaginacionQueryDto) {
    return await this.productRepository.listar(paginacionQueryDto)
  }

  async getImagen(fileName: string) {
    const path = join('uploads', fileName)

    const imageBuffer = await fs.promises.readFile(path)
    const base64String = imageBuffer.toString('base64')
    const typeImage = extname(fileName).split('.')
    return [
      {
        imgType: `image/${typeImage[1]}`,
        path: fileName,
        base64: base64String,
      },
    ]
  }

  async updateProduct(
    id: string,
    imagenes: Array<Express.Multer.File>,
    productoDto: ActualizarProductoDto,
    usuarioAuditoria: string
  ) {
    let nombreImagen = [productoDto.imagen]
    if (imagenes) {
      nombreImagen = await this.fileService.saveImages(imagenes)
    }

    const product = await this.productRepository.buscarPorId(id)
    if (!product) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    await this.productRepository.actualizar(
      id,
      productoDto,
      usuarioAuditoria,
      nombreImagen[0]
    )
    return { id }
  }

  async active(idParametro: string, usuarioAuditoria: string) {
    const producto = await this.productRepository.buscarPorId(idParametro)
    if (!producto) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    const productoDto = new ActualizarProductoDto()
    productoDto.estado = ProductoEstado.ACTIVO
    const nameImageProducto = productoDto.imagen
    await this.productRepository.actualizar(
      idParametro,
      productoDto,
      usuarioAuditoria,
      nameImageProducto
    )
    return {
      id: idParametro,
      estado: productoDto.estado,
    }
  }

  async inactive(idProduct: string, usuarioAuditoria: string) {
    const product = await this.productRepository.buscarPorId(idProduct)
    if (!product) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    const productoDto = new ActualizarProductoDto()
    productoDto.estado = ProductoEstado.INACTIVO
    const nameImageProducto = productoDto.imagen
    await this.productRepository.actualizar(
      idProduct,
      productoDto,
      usuarioAuditoria,
      nameImageProducto
    )
    return {
      id: idProduct,
      estado: productoDto.estado,
    }
  }
}
