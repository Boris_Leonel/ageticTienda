import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common'
import { ProductService } from '../service/producto.service'
import { JwtAuthGuard } from '@/core/authentication/guards/jwt-auth.guard'
import { PaginacionQueryDto } from '@/common/dto/paginacion-query.dto'
import { BaseController } from '@/common/base'
import { ParamIdDto } from '@/common/dto/params-id.dto'
import { Request } from 'express'
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiProperty,
  ApiTags,
} from '@nestjs/swagger'
import { FilesInterceptor } from '@nestjs/platform-express'
import { CrearProductoDto } from '../dto/crear-producto.dto'
import { ActualizarProductoDto } from '../dto/actualizar-producto.dto'

@ApiTags('Productos')
@ApiBearerAuth()
@Controller('productos')
// @UseGuards(JwtAuthGuard, CasbinGuard)
@UseGuards(JwtAuthGuard)
export class ProductController extends BaseController {
  constructor(private productService: ProductService) {
    super()
  }

  @ApiOperation({ summary: 'API para obtener el listado de productos' })
  @Get()
  async listar(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.productService.list(paginacionQueryDto)
    return this.successListRows(result)
  }

  @Get('/imagen/:filename')
  async getUploadedImage(@Param('filename') filename: string) {
    const result = await this.productService.getImagen(filename)
    return this.success(result)
  }

  @ApiOperation({ summary: 'API para crear un nuevo producto' })
  @ApiBody({
    type: CrearProductoDto,
    description:
      'Esta API permite crear un nuevo producto utilizando los datos proporcionados en el cuerpo de la solicitud.',
    required: true,
  })
  @Post()
  @UseInterceptors(FilesInterceptor('imagen[]'))
  async crear(
    @UploadedFiles() imagen: Array<Express.Multer.File>,
    @Req() req: Request,
    @Body() productoDto: CrearProductoDto
  ) {
    const usuarioAuditoria = this.getUser(req)
    const result = await this.productService.create(
      imagen,
      productoDto,
      usuarioAuditoria
    )
    return this.successCreate(result)
  }

  @ApiOperation({ summary: 'API para actualizar un producto' })
  @ApiProperty({
    type: ParamIdDto,
  })
  @ApiBody({
    type: ActualizarProductoDto,
    description:
      'Esta API permite actualizar un producto existente utilizando los atributos proporcionados en el cuerpo de la solicitud.',
    required: true,
  })
  @Patch(':id')
  @UseInterceptors(FilesInterceptor('imagen[]'))
  async actualizar(
    @Param() params: ParamIdDto,
    @UploadedFiles() imagen: Array<Express.Multer.File>,
    @Req() req: Request,
    @Body() productoDto: ActualizarProductoDto
  ) {
    const { id: idProducto } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.productService.updateProduct(
      idProducto,
      imagen,
      productoDto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @ApiOperation({ summary: 'API para activar un producto' })
  @ApiProperty({
    type: ParamIdDto,
  })
  @Patch('/:id/activacion')
  async activar(@Req() req: Request, @Param() params: ParamIdDto) {
    const { id: idProducto } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.productService.active(
      idProducto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @ApiOperation({ summary: 'API para inactivar un producto' })
  @ApiProperty({
    type: ParamIdDto,
  })
  @Patch('/:id/inactivacion')
  async inactivar(@Req() req: Request, @Param() params: ParamIdDto) {
    const { id: idProduct } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.productService.inactive(
      idProduct,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }
}
