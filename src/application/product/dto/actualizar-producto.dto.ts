import { IsNotEmpty } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class ActualizarProductoDto {
  @ApiProperty({ example: 'Galletas' })
  nombre: string

  @ApiProperty({ example: 'GALL-001' })
  codigo: string

  @ApiProperty({ example: '12.99' })
  precio: number

  @ApiProperty({ example: 'Este producto tiene una descripción' })
  descripcion: string

  @ApiProperty({ example: '5' })
  stock: number

  @ApiProperty({ example: '2' })
  stock_min: number

  @ApiProperty({ example: '10' })
  stock_max: number

  @ApiProperty({ example: 'imagen-de-pan.jpg' })
  imagen: string

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  idCategoria: number

  @ApiProperty({ example: '2' })
  @IsNotEmpty()
  idUnidad: number

  @ApiProperty({ example: 'ACTIVO' })
  estado?: string
}
