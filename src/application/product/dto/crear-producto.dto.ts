import { IsNotEmpty, IsOptional } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class CrearProductoDto {
  @ApiProperty({ example: 'Pan Chamillo' })
  @IsNotEmpty()
  nombre: string

  @ApiProperty({ example: 'PAN-002' })
  @IsNotEmpty()
  codigo: string

  @ApiProperty({ example: '12.5' })
  @IsNotEmpty()
  precio: number

  @ApiProperty({ example: 'Esto es una descripción del pan chamillo' })
  @IsNotEmpty()
  descripcion: string

  @ApiProperty({ example: '103' })
  @IsNotEmpty()
  stock: number

  @ApiProperty({ example: '50' })
  @IsNotEmpty()
  stock_min: number

  @ApiProperty({ example: '120' })
  @IsNotEmpty()
  stock_max: number

  @ApiProperty({ example: 'imagen-de-pan.jpg' })
  @IsOptional()
  imagen?: string

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  idCategoria: number

  @ApiProperty({ example: '2' })
  @IsNotEmpty()
  idUnidad: number

  @ApiProperty({ example: 'ACTIVO' })
  @IsOptional()
  estado?: string
}

export class RespuestaCrearParametroDto {
  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  id: string
}
