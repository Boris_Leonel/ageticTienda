import { Module } from '@nestjs/common'
import { ProductController } from '@/application/product/controller/product.controller'
import { ProductService } from '@/application/product/service/producto.service'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ProductoRepository } from '@/application/product/repository/producto.repository'
import { Producto } from './entity'
import { FileService } from '@/common/lib/file.service'

@Module({
  controllers: [ProductController],
  providers: [FileService, ProductService, ProductoRepository],
  imports: [TypeOrmModule.forFeature([Producto])],
})
export class ProductoModule {}
