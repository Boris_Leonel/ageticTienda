import { Brackets, DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { Producto } from '../entity'
import { PaginacionQueryDto } from '@/common/dto/paginacion-query.dto'
import { CrearProductoDto } from '../dto/crear-producto.dto'
import { ActualizarProductoDto } from '../dto'

@Injectable()
export class ProductoRepository {
  constructor(private dataSource: DataSource) {}

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .where({ id: id })
      .getOne()
  }

  async actualizar(
    id: string,
    productoDto: ActualizarProductoDto,
    usuarioAuditoria: string,
    imagenGuardar: string
  ) {
    const datosActualizar = new Producto({
      ...productoDto,
      usuarioModificacion: usuarioAuditoria,
      imagen: imagenGuardar,
    })
    return await this.dataSource
      .getRepository(Producto)
      .update(id, datosActualizar)
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar, filtro, orden, sentido } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .leftJoinAndSelect('producto.categoria', 'categoria')
      .leftJoinAndSelect('producto.unidad', 'unidad')
      .select([
        'producto.id',
        'producto.codigo',
        'producto.nombre',
        'producto.precio',
        'producto.descripcion',
        'producto.stock',
        'producto.stock_min',
        'producto.stock_max',
        'producto.imagen',
        'categoria.id',
        'categoria.nombre',
        'unidad.id',
        'unidad.nombre',
        'producto.estado',
      ])
      // .where('producto.estado = :estado', { estado: ProductoEstado.ACTIVO })
      .take(limite)
      .skip(saltar)

    switch (orden) {
      case 'codigo':
        query.addOrderBy('producto.codigo', sentido)
        break
      case 'nombre':
        query.addOrderBy('producto.nombre', sentido)
        break
      case 'precio':
        query.addOrderBy('producto.precio', sentido)
        break
      case 'descripcion':
        query.addOrderBy('producto.descripcion', sentido)
        break
      case 'stock_min':
        query.addOrderBy('producto.stock_min', sentido)
        break
      case 'stock_max':
        query.addOrderBy('producto.stock_max', sentido)
        break
      case 'stock':
        query.addOrderBy('producto.stock', sentido)
        break
      case 'categoria':
        query.addOrderBy('categoria.nombre', sentido)
        break
      case 'unidad':
        query.addOrderBy('unidad.nombre', sentido)
        break
      case 'estado':
        query.addOrderBy('producto.estado', sentido)
        break
      default:
        query.orderBy('producto.id', 'ASC')
    }

    if (filtro) {
      const isNumber = !isNaN(Number(filtro))

      query.andWhere(
        new Brackets((qb) => {
          qb.orWhere('producto.codigo like :filtro', { filtro: `%${filtro}%` })
          qb.orWhere('producto.nombre ilike :filtro', { filtro: `%${filtro}%` })
          qb.orWhere('producto.descripcion ilike :filtro', {
            filtro: `%${filtro}%`,
          })

          if (isNumber) {
            const numberFiltro = Number(filtro)
            qb.orWhere('producto.precio = :numberFiltro', { numberFiltro })
            qb.orWhere('producto.stock = :numberFiltro', { numberFiltro })
            qb.orWhere('producto.stock_min = :numberFiltro', { numberFiltro })
            qb.orWhere('producto.stock_max = :numberFiltro', { numberFiltro })
          }

          qb.orWhere('categoria.nombre ilike :filtro', {
            filtro: `%${filtro}%`,
          })
          qb.orWhere('unidad.nombre ilike :filtro', { filtro: `%${filtro}%` })
        })
      )
    }
    return await query.getManyAndCount()
  }

  buscarCodigo(codigo: string) {
    return this.dataSource
      .getRepository(Producto)
      .findOne({ where: { codigo: codigo } })
  }

  async crear(
    parametroDto: CrearProductoDto,
    usuarioAuditoria: string,
    imagenGuardar: string
  ) {
    const {
      nombre,
      codigo,
      precio,
      descripcion,
      stock,
      stock_min,
      stock_max,
      idCategoria,
      idUnidad,
    } = parametroDto

    const producto = new Producto()
    producto.nombre = nombre
    producto.codigo = codigo
    producto.precio = precio
    producto.imagen = imagenGuardar
    producto.descripcion = descripcion
    producto.stock = stock
    producto.stock_min = stock_min
    producto.stock_max = stock_max
    producto.idCategoria = idCategoria
    producto.idUnidad = idUnidad
    producto.usuarioCreacion = usuarioAuditoria

    return await this.dataSource.getRepository(Producto).save(producto)
  }
}
