import { UtilService } from '@/common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '@/common/entity/auditoria.entity'
import { ProductoEstado } from '../constant'
import { Categoria } from '@/application/categoria-producto/entity'
import { Unidad } from '@/application/unidad/entity'
import { DetalleVenta } from '@/application/detalle-venta/entity'
import { DetalleCarrito } from '@/application/detalle-carrito/entity'

dotenv.config()

@Check(UtilService.buildStatusCheck(ProductoEstado))
@Entity({ name: 'productos', schema: process.env.DB_SCHEMA })
export class Producto extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Parámetro',
  })
  id: string

  @Column({
    length: 50,
    type: 'varchar',
    comment: 'Nombre de producto',
  })
  nombre: string

  @Column({
    length: 50,
    type: 'varchar',
    comment: 'Codigo de producto',
  })
  codigo: string

  @Column({
    type: 'decimal',
    comment: 'Precio de producto',
  })
  precio: number

  @Column({
    type: 'text',
    comment: 'Descripción de producto',
  })
  descripcion: string

  @Column({ type: 'bigint', comment: 'Stock' })
  stock: number

  @Column({ type: 'bigint', comment: 'Stock minimo' })
  stock_min: number

  @Column({ type: 'bigint', comment: 'Stock maximo' })
  stock_max: number

  @Column({
    type: 'varchar',
    comment: 'Imagen de producto',
    nullable: true,
  })
  imagen: string

  @Column({
    name: 'id_categoria_producto',
    type: 'bigint',
    nullable: false,
    comment: 'clave foránea que referencia la tabla de categoria productos',
  })
  idCategoria: number

  @Column({
    name: 'id_unidad',
    type: 'bigint',
    nullable: false,
    comment: 'clave foránea que referencia la tabla de unidad',
  })
  idUnidad: number

  @ManyToOne(() => Categoria, (categoria) => categoria.productos, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_categoria_producto',
    referencedColumnName: 'id',
  })
  categoria: Categoria

  @ManyToOne(() => Unidad, (unidad) => unidad.productos, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_unidad',
    referencedColumnName: 'id',
  })
  unidad: Unidad

  @OneToMany(() => DetalleVenta, (detalleVenta) => detalleVenta.producto)
  detalleVentas: DetalleVenta[]

  @OneToMany(
    () => DetalleCarrito,
    (detallesCarrito) => detallesCarrito.producto
  )
  detalleCarrito: DetalleCarrito[]

  constructor(data?: Partial<Producto>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || ProductoEstado.ACTIVO
  }
}
