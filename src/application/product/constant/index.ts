import { Status } from '@/common/constants'

export enum ProductoEstado {
  ACTIVO = Status.ACTIVE,
  INACTIVO = Status.INACTIVE,
}
