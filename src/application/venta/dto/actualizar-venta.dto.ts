import { IsNotEmpty } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'
import { IsOptional } from 'class-validator'

export class ActualizarVentaDto {
  @ApiProperty({ example: 'CP-001' })
  @IsNotEmpty()
  numComprobante: string

  @ApiProperty({ example: '05-03-2024' })
  @IsNotEmpty()
  fecha: string

  @ApiProperty({ example: '105.99' })
  @IsNotEmpty()
  total: string

  @ApiProperty({ example: 'Venta articulos' })
  @IsOptional()
  descripcion: string

  @ApiProperty({ example: 'Tarjeta' })
  tipoPago: string

  @ApiProperty({ example: 'ACTIVO' })
  estado?: string
}
