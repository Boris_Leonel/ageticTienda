import { IsNotEmpty, IsOptional } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class CrearVentaDto {
  @ApiProperty({ example: 'CP-001' })
  @IsNotEmpty()
  numComprobante: string

  @ApiProperty({ example: '05-03-2024' })
  @IsNotEmpty()
  fecha: Date

  @ApiProperty({ example: '105.99' })
  @IsNotEmpty()
  total: string

  @ApiProperty({ example: 'Venta articulos' })
  @IsOptional()
  descripcion: string

  @ApiProperty({ example: 'Tarjeta' })
  tipoPago: string

  @ApiProperty({ example: 'ACTIVO' })
  @IsOptional()
  estado?: string
}

export class RespuestaCrearVentaDto {
  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  id: string
}
