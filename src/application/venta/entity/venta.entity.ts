import {
  BeforeInsert,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '@/common/entity/auditoria.entity'
import { DetalleVenta } from '@/application/detalle-venta/entity'
import { VentaEstado } from '../constant'

dotenv.config()

// @Check(UtilService.buildStatusCheck(VentaEstado))
@Entity({ name: 'ventas', schema: process.env.DB_SCHEMA })
export class Venta extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Ventas',
  })
  id: string

  @Column({
    name: 'num_comprobante',
    length: 15,
    type: 'varchar',
    comment: 'Numero de comprobante de venta',
  })
  numComprobante: string | null

  @Column({
    type: 'timestamp without time zone',
    nullable: true,
    default: () => 'now()',
    comment: 'Fecha de la venta',
  })
  fecha?: Date | null

  @Column({
    length: 15,
    type: 'varchar',
    comment: 'Total de la venta',
  })
  total: string

  @Column({
    name: 'descripcion',
    length: 255,
    type: 'varchar',
    comment: 'Descripción de la venta',
  })
  descripcion: string

  @Column({
    length: 255,
    name: 'tipo_pago',
    type: 'varchar',
    comment: 'Tipo de pago de venta',
  })
  tipoPago: string

  @OneToMany(() => DetalleVenta, (detalleVenta) => detalleVenta.venta)
  detalleVenta: DetalleVenta[]

  constructor(data?: Partial<Venta>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || VentaEstado.ACTIVO
  }
}
