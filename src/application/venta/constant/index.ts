import { Status } from '@/common/constants'

export enum VentaEstado {
  ACTIVO = Status.ACTIVE,
  INACTIVO = Status.INACTIVE,
}
