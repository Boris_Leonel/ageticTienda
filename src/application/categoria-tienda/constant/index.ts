import { Status } from '@/common/constants'

export enum CategoriaTiendaEstado {
  ACTIVO = Status.ACTIVE,
  INACTIVO = Status.INACTIVE,
}
