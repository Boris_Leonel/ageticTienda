import { UtilService } from '@/common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '@/common/entity/auditoria.entity'
import { CategoriaTiendaEstado } from '../constant'
import { CategoriaProducto } from '@/application/categoria-producto/entity/categoria-producto.entity'
import { Tienda } from '@/application/tienda/entity'

dotenv.config()

@Check(UtilService.buildStatusCheck(CategoriaTiendaEstado))
@Entity({ name: 'categorias_tienda', schema: process.env.DB_SCHEMA })
export class CategoriaTienda extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla categoría tienda',
  })
  id: string

  @Column({
    length: 50,
    type: 'varchar',
    comment: 'Nombre de categoría',
  })
  nombre: string

  @Column({
    length: 255,
    type: 'varchar',
    comment: 'Descripción de categoría',
  })
  descripcion: string

  @Column({
    name: 'id_tienda',
    type: 'bigint',
    nullable: false,
    comment: 'clave foránea que referencia la tabla tienda',
  })
  idTienda: number

  @OneToMany(
    () => CategoriaProducto,
    (categoriaProducto) => categoriaProducto.categoriaTienda
  )
  categoriaProducto: CategoriaProducto[]

  @ManyToOne(() => Tienda, (tienda) => tienda.categoriasTienda, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_tienda',
    referencedColumnName: 'id',
  })
  tienda: Tienda

  constructor(data?: Partial<CategoriaTienda>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || CategoriaTiendaEstado.ACTIVO
  }
}
