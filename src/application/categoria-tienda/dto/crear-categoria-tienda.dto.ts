import { IsNotEmpty, IsOptional } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class CrearCategoriaTiendaDto {
  @ApiProperty({ example: 'Tienda de Ropa' })
  nombre: string

  @ApiProperty({ example: 'Tienda dedicada a la venta de ropa' })
  descripcion: string

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  idTienda: string

  @ApiProperty({ example: 'ACTIVO' })
  @IsOptional()
  estado?: string
}

export class RespuestaCrearCategoriaTienda {
  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  id: string
}
