import { UtilService } from '@/common/lib/util.service'
import {
  BeforeInsert,
  Check,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm'
import dotenv from 'dotenv'
import { AuditoriaEntity } from '@/common/entity/auditoria.entity'
import { TiendaEstado } from '../constant'
import { CategoriaTienda } from '@/application/categoria-tienda/entity/categoria-tienda.entity'
import { Carrito } from '@/application/carrito/entity'

dotenv.config()

@Check(UtilService.buildStatusCheck(TiendaEstado))
@Entity({ name: 'tiendas', schema: process.env.DB_SCHEMA })
export class Tienda extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'id',
    comment: 'Clave primaria de la tabla Parámetro',
  })
  id: number

  @Column({
    length: 255,
    type: 'varchar',
    unique: true,
    comment: 'Dirección de la tienda',
  })
  direccion: string

  @Column({
    length: 50,
    type: 'varchar',
    comment: 'Telefono del gerente',
  })
  telefono: string

  @Column({
    length: 15,
    type: 'varchar',
    comment: 'Nombre de gerente',
  })
  gerente: string

  @OneToMany(() => CategoriaTienda, (categoriaTienda) => categoriaTienda.tienda)
  categoriasTienda: CategoriaTienda[]

  @OneToMany(() => Carrito, (carritos) => carritos.tienda)
  carritos: Carrito[]

  constructor(data?: Partial<Tienda>) {
    super(data)
  }

  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || TiendaEstado.ACTIVO
  }
}
