import { Status } from '@/common/constants'

export enum TiendaEstado {
  ACTIVO = Status.ACTIVE,
  INACTIVO = Status.INACTIVE,
}
