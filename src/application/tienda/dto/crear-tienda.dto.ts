import { IsNotEmpty, IsOptional } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class CrearTiendaDto {
  @ApiProperty({ example: 'Calle 5' })
  @IsNotEmpty()
  direccion: string

  @ApiProperty({ example: '68475123' })
  @IsNotEmpty()
  telefono: string

  @ApiProperty({ example: 'Perez Ochoa' })
  @IsNotEmpty()
  gerente: string

  @ApiProperty({ example: 'ACTIVO' })
  @IsOptional()
  estado?: string
}

export class RespuestaCrearParametroDto {
  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  id: string
}
