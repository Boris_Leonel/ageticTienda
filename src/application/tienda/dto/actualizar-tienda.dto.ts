import { IsNotEmpty } from '@/common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class ActualizarTiendaDto {
  @ApiProperty({ example: 'Calle 5' })
  @IsNotEmpty()
  direccion: string

  @ApiProperty({ example: '68475123' })
  @IsNotEmpty()
  telefono: string

  @ApiProperty({ example: 'Perez Ochoa' })
  @IsNotEmpty()
  gerente: string

  @ApiProperty({ example: 'ACTIVO' })
  estado?: string
}
