import { Injectable } from '@nestjs/common'
import * as fs from 'fs'
import { TextService } from './text.service'

@Injectable()
export class FileService {
  /**
   * Método para guardar un array de imagenes y retorna los nombres de cada imagen
   * @param images
   */
  async saveImages(images): Promise<string[]> {
    const nombresImagenes: string[] = []

    for (const imagen of images) {
      const uuid = TextService.generateUuid()
      const extension = this.obtenerExtension(imagen.originalname)
      const nombreImagen = `${uuid}.${extension}`
      const rutaDirectorio = './uploads'
      const rutaImagen = `${rutaDirectorio}/${nombreImagen}`

      await this.garantizarDirectorioExiste(rutaDirectorio)
      await fs.promises.writeFile(rutaImagen, imagen.buffer)

      nombresImagenes.push(nombreImagen)
    }
    return nombresImagenes
  }

  /**
   * Método para obtener la extension de la imagen
   * @param nombreArchivo
   */
  obtenerExtension(nombreArchivo: string): string {
    const partesNombre = nombreArchivo.split('.')
    return partesNombre[partesNombre.length - 1]
  }

  /**
   * Método para verificar si existe una ruta en directorio
   * @param rutaDirectorio
   */
  async garantizarDirectorioExiste(rutaDirectorio: string): Promise<void> {
    try {
      await fs.promises.access(rutaDirectorio, fs.constants.F_OK)
    } catch (error) {
      if (error.code === 'ENOENT') {
        await fs.promises.mkdir(rutaDirectorio, { recursive: true })
      } else {
        throw error
      }
    }
  }
}
